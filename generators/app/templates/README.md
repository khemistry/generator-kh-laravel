# <%= name %>

## All Laravel documentation can be found at [https://laravel.com/docs/](https://laravel.com/docs/)

<% if (docker) { %>
## Build the Docker image
Run `docker build -t example:latest .`

## Using Docker container locally
Run `docker run -p 8080:80 example:latest`
<% } %>

<% if (pipelines) { %>
## Bitbucket Pipelines environment variables
The following variables are required by pipelines:
- **AWS_ACCESS_KEY_ID**
  - IAM user cli access credentials
- **AWS_DEFAULT_REGION**
  - default: ap-southeast-2
- **AWS_SECRET_ACCESS_KEY**
  - IAM user cli access creds
- **S3_ENV_BUCKET**
  - Name of the S3 bucket where env's files for projects are kept
- **PROJECT_NAME**
  - Bitbucket project name
- **SERVICE_NAME**
  - The repo slug or service name, e.g.: backend
- **PROJECT_ECR_URL**
  - The AWS ECR Docker endpoint/url sans the service name
<% } %>
