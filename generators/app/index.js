var Generator = require('yeoman-generator');
var rimraf = require('rimraf');
var replace = require('replace-in-file');

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this._copy = this._copy.bind(this);
  }

  _copy(file) {
    this.fs.copy(this.templatePath(file), file);
  }

  _clone(files) {
    files.forEach(this._copy);
  }

  async prompting() {
    this.answers = await this.prompt([
      {
        type: 'input',
        name: 'name',
        message: 'Your project name:',
        default: this.appname
      },
      {
        type: 'confirm',
        name: 'docker',
        message: 'Setup a Dockerfile?',
        default: false,
      },
      {
        type: 'confirm',
        name: 'pipelines',
        message: 'Setup Bitbucket Pipelines?',
        default: false,
      },
      {
        type: 'confirm',
        name: 'start',
        message: 'Serve application once setup?',
        default: true,
      }
    ]);
  }

  writing() {
    this.spawnCommandSync('composer', ['create-project', '--prefer-dist', 'laravel/laravel', '.']);
    this.spawnCommandSync('git', ['init']);

    rimraf.sync('package.json');
    rimraf.sync('resources/js');
    rimraf.sync('resources/sass');
    rimraf.sync('resources/views/*');
    rimraf.sync('routes/*');
    rimraf.sync('resources/views/welcome.blade.php');
    rimraf.sync('webpack.mix.js');
    rimraf.sync('readme.md');

    this.fs.writeJSON('package.json', {
      private: true,
      scripts: {
          dev: 'npm run development',
          development: 'cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js',
          watch: 'npm run development -- --watch',
          'watch-poll': 'npm run watch -- --watch-poll',
          hot: 'cross-env NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --config=node_modules/laravel-mix/setup/webpack.config.js',
          prod: 'npm run production',
          production: 'cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js'
      }
    });

    this._clone([
      'webpack.mix.js',
      'public/.gitignore',
      'app/Http/Controllers/Welcome/WelcomeController.php',
      'routes/api.php',
      'routes/channels.php',
      'routes/console.php',
      'routes/web.php',
      'resources/js/app.js',
      'resources/sass/app.scss',
      'resources/svg/403.svg',
      'resources/svg/404.svg',
      'resources/svg/500.svg',
      'resources/svg/503.svg',
    ]);

    this.fs.copyTpl(
      this.templatePath('resources/views/welcome/index.blade.php'),
      this.destinationPath('resources/views/welcome/index.blade.php'),
      {
        name: this.answers.name
      }
    );

    this.fs.copyTpl(
      this.templatePath('README.md'),
      this.destinationPath('README.md'),
      {
        name: this.answers.name,
        docker: this.answers.docker,
        pipelines: this.answers.pipelines,
      }
    );

    replace.sync({
      files: '.env',
      from: /Laravel/g,
      to: this.answers.name,
    });

    if (this.answers.docker) {
      this.fs.copy(
        this.templatePath('Dockerfile'),
        this.destinationPath('Dockerfile')
      );

      this.fs.copy(
        this.templatePath('.docker'),
        this.destinationPath('.docker')
      );
    }

    if (this.answers.pipelines) {
      this.fs.copy(
        this.templatePath('bitbucket-pipelines.yml'),
        this.destinationPath('bitbucket-pipelines.yml')
      );
    }
  }

  install() {
    this.npmInstall([
      'laravel-mix',
      'cross-env'
    ], { 'save': true });
  }

  end() {
      this.spawnCommandSync('git', ['clean', '-X', '-d', '-f', 'public/']);
      this.spawnCommandSync('git', ['add', '.']);
      this.spawnCommandSync('git', ['commit', '-m', 'Sets up baseline Laravel project']);

      if(this.answers.start) {
        this.spawnCommandSync('npm', ['run', 'dev']);
        this.spawnCommandSync('php', ['artisan', 'serve']);
      }
  }
};
